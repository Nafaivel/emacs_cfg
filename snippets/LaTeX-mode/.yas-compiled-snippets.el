;;; Compiled snippets and support files for `LaTeX-mode'
;;; Snippet definitions:
;;;
(yas-define-snippets 'LaTeX-mode
		     '(("subsub" "~\n\n\\subsubsection{$1}\n\\setcounter{figure}{0}\n\n~\n\n$0" "subsub" nil nil nil "/home/antasa/.config/emacs/snippets/LaTeX-mode/subsub" nil nil)
		       ("sub" "~\n\n\\subsection{$1}\n\\setcounter{figure}{0}\n\n~\n\n$0" "sub" nil nil nil "/home/antasa/.config/emacs/snippets/LaTeX-mode/sub" nil nil)
		       ("sec" "\\newpage\n\\section{$1}\n\n$0\n" "section" nil nil nil "/home/antasa/.config/emacs/snippets/LaTeX-mode/sec" nil nil)
		       ("itemize" "\\begin{itemize}\n  \\item $1\n \\end{itemize}\n $0" "itemize" nil nil nil "/home/antasa/.config/emacs/snippets/LaTeX-mode/itemize" nil nil)
		       ("fi" "\\begin{figure}[ht!]\n  \\centering\n  \\includegraphics[width=15cm]{./images/$1}\n  \\caption{ $2 }\n\\end{figure}\n\n$0" "figure" nil nil nil "/home/antasa/.config/emacs/snippets/LaTeX-mode/fig" nil nil)))


;;; Do not edit! File generated at Mon May  1 21:15:03 2023
